﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ShoesBazaar.web.Startup))]
namespace ShoesBazaar.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
